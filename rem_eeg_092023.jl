@info "Loading CSV"
using CSV
@info "Loading DataFrames"
using DataFrames
@info "Loading Dates"
using Dates
@info "Loading Plots"
using Plots
@info "Loading Statistics"
using Statistics
@info "Loading NeuroAnalyzer"
using NeuroAnalyzer

df = CSV.read("rem_new.csv", DataFrame)

eeg_powers = DataFrame(:id=>Int64[], :rem_delta_F4A1=>Float64[], :rem_delta_F3A2=>Float64[], :rem_delta_C4A1=>Float64[], :rem_delta_C3A2=>Float64[], :rem_delta_O2A1=>Float64[], :rem_delta_O1A2=>Float64[], :rem_theta_F4A1=>Float64[], :rem_theta_F3A2=>Float64[], :rem_theta_C4A1=>Float64[], :rem_theta_C3A2=>Float64[], :rem_theta_O2A1=>Float64[], :rem_theta_O1A2=>Float64[], :rem_alpha_F4A1=>Float64[], :rem_alpha_F3A2=>Float64[], :rem_alpha_C4A1=>Float64[], :rem_alpha_C3A2=>Float64[], :rem_alpha_O2A1=>Float64[], :rem_alpha_O1A2=>Float64[], :rem_beta_F4A1=>Float64[], :rem_beta_F3A2=>Float64[], :rem_beta_C4A1=>Float64[], :rem_beta_C3A2=>Float64[], :rem_beta_O2A1=>Float64[], :rem_beta_O1A2=>Float64[], :nrem_delta_F4A1=>Float64[], :nrem_delta_F3A2=>Float64[], :nrem_delta_C4A1=>Float64[], :nrem_delta_C3A2=>Float64[], :nrem_delta_O2A1=>Float64[], :nrem_delta_O1A2=>Float64[], :nrem_theta_F4A1=>Float64[], :nrem_theta_F3A2=>Float64[], :nrem_theta_C4A1=>Float64[], :nrem_theta_C3A2=>Float64[], :nrem_theta_O2A1=>Float64[], :nrem_theta_O1A2=>Float64[], :nrem_alpha_F4A1=>Float64[], :nrem_alpha_F3A2=>Float64[], :nrem_alpha_C4A1=>Float64[], :nrem_alpha_C3A2=>Float64[], :nrem_alpha_O2A1=>Float64[], :nrem_alpha_O1A2=>Float64[], :nrem_beta_F4A1=>Float64[], :nrem_beta_F3A2=>Float64[], :nrem_beta_C4A1=>Float64[], :nrem_beta_C3A2=>Float64[], :nrem_beta_O2A1=>Float64[], :nrem_beta_O1A2=>Float64[])

rem_epochs = 0
nrem_epochs = 0

for pt_idx in 63:nrow(df)
    @info "Processing subject: $pt_idx"
    pt = df[pt_idx, :]
    seg_name = Vector{String}()
    seg_time = Vector{Time}()
    for seg_idx in 1:176
        if typeof(df[pt_idx, "SEG_$(seg_idx - 1)"]) == Missing
            break
        else
            push!(seg_name, df[pt_idx, "SEG_$(seg_idx - 1)"])
            push!(seg_time, df[pt_idx, "TIME_$(seg_idx - 1)"])
        end
    end
    
    # duration in seconds
    seg_duration = zeros(length(seg_time))
    for seg_idx in length(seg_time):-1:2
        if seg_time[seg_idx] >= seg_time[seg_idx - 1]
            seg_duration[seg_idx] = Dates.value(seg_time[seg_idx] - seg_time[seg_idx - 1]) / 10^9
        else
            seg_duration[seg_idx] = Dates.value((Time("23:59:59", "HH:MM:SS") - seg_time[seg_idx - 1]) + seg_time[seg_idx]) / 10^9
        end
    end
    
    # first segment
    t_h = parse(Int64, pt["start_t"][1:2])
    t_m = parse(Int64, pt["start_t"][4:5])
    t_s = parse(Int64, pt["start_t"][7:8])
    pt["start_t"][end-1:end] == "PM" && (t_h += 12)
    (pt["start_t"][end-1:end] == "AM" && t_h == 12) && (t_h = 0)
    t_start = Time("$t_h:$t_m:$t_s", "HH:MM:SS")
    if seg_time[1] >= t_start
        seg_duration[1] = Dates.value(seg_time[1] - t_start) / 10^9
    else
        seg_duration[1] = Dates.value(Time("23:59:59", "HH:MM:SS") - t_start + seg_time[1] ) / 10^9
    end

    for seg_idx in length(seg_name):-1:1
        if seg_duration[seg_idx] == 0
            deleteat!(seg_duration, seg_idx)
            deleteat!(seg_name, seg_idx)
        end
    end

    time_segments = Tuple{Real, Real}[]
    push!(time_segments, (0, seg_duration[1]))
    for seg_idx in 2:length(seg_name)
        push!(time_segments, (time_segments[seg_idx - 1][2], time_segments[seg_idx - 1][2] + seg_duration[seg_idx]))
    end

#     file_name = lpad(pt["patient"], 3, "0") * ".txt"
#     f = open(file_name, "w")
#     println(f, "EEG\t$(pt["eeg"])")
#     for seg_idx in 1:length(seg_name)
#         println(f, "$(seg_name[seg_idx])\t$(seg_duration[seg_idx])")
#     end
#     close(f)

    eeg_id = pt["eeg"]
    @info "Processing EDF file #$eeg_id"
    eeg = import_alice4("$eeg_id.edf")
    # keep_channel!(eeg, ch=1:6)
    # NeuroAnalyzer.filter!(eeg, fprototype=:fir, ftype=:lp, cutoff=30)
    nrem_tmp = Vector{NeuroAnalyzer.NEURO}()
    rem_tmp = Vector{NeuroAnalyzer.NEURO}()
    for seg_idx in 1:length(seg_name)
        @info "Segment: $seg_idx"
        if seg_name[seg_idx] !== "WK" && seg_name[seg_idx] !== "REM"
            # S1 / S2 / S3
            if time_segments[seg_idx][2] < signal_len(eeg) / sr(eeg)
                tmp = trim(eeg, seg=time_segments[seg_idx], inverse=true, remove_epochs=false)
                signal_len(tmp) / sr(tmp) > 20 && push!(nrem_tmp, epoch(tmp, ep_len=20))
            end
        elseif seg_name[seg_idx] == "REM"
            # REM
            if time_segments[seg_idx][2] < signal_len(eeg) / sr(eeg)
                tmp = trim(eeg, seg=time_segments[seg_idx], inverse=true, remove_epochs=false)
                signal_len(tmp) / sr(tmp) > 20 && push!(rem_tmp, epoch(tmp, ep_len=20))
            end
        end
    end

    nrem_n = length(nrem_tmp)
    rem_n = length(rem_tmp)

    global rem_epochs += rem_n
    global nrem_epochs += nrem_n

    delta_pow = zeros(6, rem_n)
    theta_pow = zeros(6, rem_n)
    alpha_pow = zeros(6, rem_n)
    beta_pow = zeros(6, rem_n)

    for seg_idx in 1:rem_n
        tmp = rem_tmp[seg_idx]
        delta_pow[:, seg_idx] = mean(band_power(tmp, f=band_frq(tmp, band=:delta)), dims=2)
        theta_pow[:, seg_idx] = mean(band_power(tmp, f=band_frq(tmp, band=:theta)), dims=2)
        alpha_pow[:, seg_idx] = mean(band_power(tmp, f=band_frq(tmp, band=:alpha)), dims=2)
        beta_pow[:, seg_idx] = mean(band_power(tmp, f=band_frq(tmp, band=:beta)), dims=2)
    end

    rem_mdelta_pow = mean(delta_pow, dims=2)[:]
    rem_mtheta_pow = mean(theta_pow, dims=2)[:]
    rem_malpha_pow = mean(alpha_pow, dims=2)[:]
    rem_mbeta_pow = mean(beta_pow, dims=2)[:]

    delta_pow = zeros(6, nrem_n)
    theta_pow = zeros(6, nrem_n)
    alpha_pow = zeros(6, nrem_n)
    beta_pow = zeros(6, nrem_n)

    for seg_idx in 1:nrem_n
        tmp = nrem_tmp[seg_idx]
        delta_pow[:, seg_idx] = mean(band_power(tmp, f=band_frq(tmp, band=:delta)), dims=2)
        theta_pow[:, seg_idx] = mean(band_power(tmp, f=band_frq(tmp, band=:theta)), dims=2)
        alpha_pow[:, seg_idx] = mean(band_power(tmp, f=band_frq(tmp, band=:alpha)), dims=2)
        beta_pow[:, seg_idx] = mean(band_power(tmp, f=band_frq(tmp, band=:beta)), dims=2)
    end

    nrem_mdelta_pow = mean(delta_pow, dims=2)[:]
    nrem_mtheta_pow = mean(theta_pow, dims=2)[:]
    nrem_malpha_pow = mean(alpha_pow, dims=2)[:]
    nrem_mbeta_pow = mean(beta_pow, dims=2)[:]

    push!(eeg_powers, [eeg_id, rem_mdelta_pow[1], rem_mdelta_pow[2], rem_mdelta_pow[3], rem_mdelta_pow[4], rem_mdelta_pow[5], rem_mdelta_pow[6], rem_mtheta_pow[1], rem_mtheta_pow[2], rem_mtheta_pow[3], rem_mtheta_pow[4], rem_mtheta_pow[5], rem_mtheta_pow[6], rem_malpha_pow[1], rem_malpha_pow[2], rem_malpha_pow[3], rem_malpha_pow[4], rem_malpha_pow[5], rem_malpha_pow[6], rem_mbeta_pow[1], rem_mbeta_pow[2], rem_mbeta_pow[3], rem_mbeta_pow[4], rem_mbeta_pow[5], rem_mbeta_pow[6], nrem_mdelta_pow[1], nrem_mdelta_pow[2], nrem_mdelta_pow[3], nrem_mdelta_pow[4], nrem_mdelta_pow[5], nrem_mdelta_pow[6], nrem_mtheta_pow[1], nrem_mtheta_pow[2], nrem_mtheta_pow[3], nrem_mtheta_pow[4], nrem_mtheta_pow[5], nrem_mtheta_pow[6], nrem_malpha_pow[1], nrem_malpha_pow[2], nrem_malpha_pow[3], nrem_malpha_pow[4], nrem_malpha_pow[5], nrem_malpha_pow[6], nrem_mbeta_pow[1], nrem_mbeta_pow[2], nrem_mbeta_pow[3], nrem_mbeta_pow[4], nrem_mbeta_pow[5], nrem_mbeta_pow[6]])

end

CSV.write("eeg_powers.csv", eeg_powers)

println("REM epochs: $rem_epochs")
println("non-REM epochs: $nrem_epochs")

@info "Analysis completed"
