using NeuroAnalyzer
using Plots
using StatsKit

cd(expanduser("~/Documents/Research/manuscripts/tdcs-avh/"))

# load EEG data
eeg1 = import_edf("79-v1.edf")
eeg2 = import_edf("79-v2.edf")

# load electrode locations, required for ICA
load_locs!(eeg1, file_name=joinpath(NeuroAnalyzer.PATH, "locs", "standard-10-20-cap19-elmiko.ced"))
load_locs!(eeg2, file_name=joinpath(NeuroAnalyzer.PATH, "locs", "standard-10-20-cap19-elmiko.ced"))

# filter
remove_powerline!(eeg1, pl_frq=50)
remove_powerline!(eeg2, pl_frq=50)
NeuroAnalyzer.filter!(eeg1, fprototype=:fir, ftype=:lp, cutoff=45, order=8)
NeuroAnalyzer.filter!(eeg2, fprototype=:fir, ftype=:lp, cutoff=45, order=8)
NeuroAnalyzer.filter!(eeg1, fprototype=:fir, ftype=:hp, cutoff=0.5, order=8)
NeuroAnalyzer.filter!(eeg2, fprototype=:fir, ftype=:hp, cutoff=0.5, order=8)

iview(eeg1)
iview(eeg2)

# remove ECG using ICA
keep_channel!(eeg1, ch=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 24])
ic, ic_mw, ic_var = ica_decompose(eeg1, n=20, ch=1:20)
p1 = NeuroAnalyzer.plot(eeg1, ch=1:4, mono=true, seg=(40, 60))
p2 = NeuroAnalyzer.plot(eeg1, ic, c_idx=1:4, mono=true, seg=(40, 60))
p = Plots.plot(p1, p2, layout=(2, 1))
# ECG in #1 component
ica_reconstruct!(eeg1, ic, ic_mw, ic_idx=1)
delete_channel!(eeg1, ch=20)

# remove ECG using ICA
keep_channel!(eeg2, ch=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 24])
ic, ic_mw, ic_var = ica_decompose(eeg2, n=20, ch=1:20)
p1 = NeuroAnalyzer.plot(eeg2, ch=1:20, mono=true, seg=(40, 60))
p2 = NeuroAnalyzer.plot(eeg2, ic, mono=true, seg=(40, 60))
p = Plots.plot(p1, p2, layout=(2, 1))
# ECG in #4 component
ica_reconstruct!(eeg2, ic, ic_mw, ic_idx=1)
delete_channel!(eeg2, ch=20)

iedit(eeg1)
save(eeg1, file_name="v1.hdf5", overwrite=true)

iedit(eeg2)
save(eeg2, file_name="v2.hdf5", overwrite=true)

# divide into epochs
epoch!(eeg1, ep_len=20)
epoch!(eeg2, ep_len=20)

iview(eeg1)
iview(eeg2)

nepochs(eeg1)
nepochs(eeg2)

# delete_epoch!(eeg1, ep=44:45)
delete_epoch!(eeg2, ep=53:59)

iedit(eeg1)
iedit(eeg2)

# CAR referencing
reference_car!(eeg1)
reference_car!(eeg2)

# save(eeg1, file_name="v1_edited.hdf5", overwrite=true)
# save(eeg2, file_name="v2_edited.hdf5", overwrite=true)

iview(eeg1)
iview(eeg2)

eeg1 = NeuroAnalyzer.load("v1_edited.hdf5")
eeg2 = NeuroAnalyzer.load("v2_edited.hdf5")

f3 = get_channel(eeg1, ch="f3")
f4 = get_channel(eeg1, ch="f4")
t3 = get_channel(eeg1, ch="t3")
t4 = get_channel(eeg1, ch="t4")

delta_1 = band_power(eeg1, f=band_frq(eeg1, band=:delta))
delta_2 = band_power(eeg2, f=band_frq(eeg2, band=:delta))
p1 = plot_topo(eeg1, delta_1, c_idx=1:19, nmethod=:zscore, imethod=:tp)
p2 = plot_topo(eeg2, delta_2, c_idx=1:19, nmethod=:zscore, imethod=:tp)
Plots.plot(p1, p2)

# analyze PSD
delta_1 = band_power(eeg1, f=band_frq(eeg1, band=:delta))
delta_2 = band_power(eeg2, f=band_frq(eeg2, band=:delta))
delta1_f3 = delta_1[f3, :]
delta2_f3 = delta_2[f3, :]
Plots.plot(delta1_f3, seriestype=:boxplot, legend=nothing, title="band: delta, location: F3", ylabel="power [dB]")
p1 = plot!(delta2_f3, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 30))
delta1_t3 = delta_1[t3, :]
delta2_t3 = delta_2[t3, :]
Plots.plot(delta1_t3, seriestype=:boxplot, legend=nothing, title="band: delta, location: T3", ylabel="power [dB]")
p2 = plot!(delta2_t3, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 30), ylabel="")
delta_plot = Plots.plot(p1, p2, fillcolor=:white, markercolor=:white, markersize=2, guidefontsize=8, tickfontsize=6, titlefontsize=10, dpi=300)
plot_save(delta_plot, file_name="delta_f3t3.png")

theta_1 = band_power(eeg1, f=band_frq(eeg1, band=:theta))
theta_2 = band_power(eeg2, f=band_frq(eeg2, band=:theta))
theta1_f3 = theta_1[f3, :]
theta2_f3 = theta_2[f3, :]
Plots.plot(theta1_f3, seriestype=:boxplot, legend=nothing, title="band: theta, location: F3", ylabel="power [dB]")
p1 = plot!(theta2_f3, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15))
theta1_t3 = theta_1[t3, :]
theta2_t3 = theta_2[t3, :]
Plots.plot(theta1_t3, seriestype=:boxplot, legend=nothing, title="band: theta, location: T3", ylabel="power [dB]")
p2 = plot!(theta2_t3, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15), ylabel="")
theta_plot = Plots.plot(p1, p2, fillcolor=:white, markercolor=:white, markersize=2, guidefontsize=8, tickfontsize=6, titlefontsize=10, dpi=300)
plot_save(theta_plot, file_name="theta_f3t3.png")

alpha_1 = band_power(eeg1, f=band_frq(eeg1, band=:alpha))
alpha_2 = band_power(eeg2, f=band_frq(eeg2, band=:alpha))
alpha1_f3 = alpha_1[f3, :]
alpha2_f3 = alpha_2[f3, :]
Plots.plot(alpha1_f3, seriestype=:boxplot, legend=nothing, title="band: alpha, location: F3", ylabel="power [dB]")
p1 = plot!(alpha2_f3, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15))
alpha1_t3 = alpha_1[t3, :]
alpha2_t3 = alpha_2[t3, :]
Plots.plot(alpha1_t3, seriestype=:boxplot, legend=nothing, title="band: alpha, location: T3", ylabel="power [dB]")
p2 = plot!(alpha2_t3, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15), ylabel="")
alpha_plot = Plots.plot(p1, p2, fillcolor=:white, markercolor=:white, markersize=2, guidefontsize=8, tickfontsize=6, titlefontsize=10, dpi=300)
plot_save(alpha_plot, file_name="alpha_f3t3.png")

beta_1 = band_power(eeg1, f=band_frq(eeg1, band=:beta))
beta_2 = band_power(eeg2, f=band_frq(eeg2, band=:beta))
beta1_f3 = beta_1[f3, :]
beta2_f3 = beta_2[f3, :]
Plots.plot(beta1_f3, seriestype=:boxplot, legend=nothing, title="band: beta, location: F3", ylabel="power [dB]")
p1 = plot!(beta2_f3, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15))
beta1_t3 = beta_1[t3, :]
beta2_t3 = beta_2[t3, :]
Plots.plot(beta1_t3, seriestype=:boxplot, legend=nothing, title="band: beta, location: T3", ylabel="power [dB]")
p2 = plot!(beta2_t3, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15), ylabel="")
beta_plot = Plots.plot(p1, p2, fillcolor=:white, markercolor=:white, markersize=2, guidefontsize=8, tickfontsize=6, titlefontsize=10, dpi=300)
plot_save(beta_plot, file_name="beta_f3t3.png")

# plot results F3 / T3
p_f3t3 = Plots.plot(delta_plot, theta_plot, alpha_plot, beta_plot, layout=(2, 2), titlefontsize=5, tickfontsize=4, guidefontsize=4, dpi=300)
plot_save(p_f3t3, file_name="f3t3.png")

# analyze PSD
delta_1 = band_power(eeg1, f=band_frq(eeg1, band=:delta))
delta_2 = band_power(eeg2, f=band_frq(eeg2, band=:delta))
delta1_f4 = delta_1[f4, :]
delta2_f4 = delta_2[f4, :]
Plots.plot(delta1_f4, seriestype=:boxplot, legend=nothing, title="band: delta, location: F4", ylabel="power [dB]")
p1 = plot!(delta2_f4, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 80))
delta1_t4 = delta_1[t4, :]
delta2_t4 = delta_2[t4, :]
Plots.plot(delta1_t4, seriestype=:boxplot, legend=nothing, title="band: delta, location: T4", ylabel="power [dB]")
p2 = plot!(delta2_t4, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 80), ylabel="")
delta_plot = Plots.plot(p1, p2, fillcolor=:white, markercolor=:white, markersize=2, guidefontsize=8, tickfontsize=6, titlefontsize=10, dpi=300)
plot_save(delta_plot, file_name="delta_f4t4.png")

theta_1 = band_power(eeg1, f=band_frq(eeg1, band=:theta))
theta_2 = band_power(eeg2, f=band_frq(eeg2, band=:theta))
theta1_f4 = theta_1[f4, :]
theta2_f4 = theta_2[f4, :]
Plots.plot(theta1_f4, seriestype=:boxplot, legend=nothing, title="band: theta, location: F4", ylabel="power [dB]")
p1 = plot!(theta2_f4, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15))
theta1_t4 = theta_1[t4, :]
theta2_t4 = theta_2[t4, :]
Plots.plot(theta1_t4, seriestype=:boxplot, legend=nothing, title="band: theta, location: T4", ylabel="power [dB]")
p2 = plot!(theta2_t4, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15), ylabel="")
theta_plot = Plots.plot(p1, p2, fillcolor=:white, markercolor=:white, markersize=2, guidefontsize=8, tickfontsize=6, titlefontsize=10, dpi=300)
plot_save(theta_plot, file_name="theta_f4t4.png")

alpha_1 = band_power(eeg1, f=band_frq(eeg1, band=:alpha))
alpha_2 = band_power(eeg2, f=band_frq(eeg2, band=:alpha))
alpha1_f4 = alpha_1[f4, :]
alpha2_f4 = alpha_2[f4, :]
Plots.plot(alpha1_f4, seriestype=:boxplot, legend=nothing, title="band: alpha, location: F4", ylabel="power [dB]")
p1 = plot!(alpha2_f4, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15))
alpha1_t4 = alpha_1[t4, :]
alpha2_t4 = alpha_2[t4, :]
Plots.plot(alpha1_t4, seriestype=:boxplot, legend=nothing, title="band: alpha, location: T4", ylabel="power [dB]")
p2 = plot!(alpha2_t4, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15), ylabel="")
alpha_plot = Plots.plot(p1, p2, fillcolor=:white, markercolor=:white, markersize=2, guidefontsize=8, tickfontsize=6, titlefontsize=10, dpi=300)
plot_save(alpha_plot, file_name="alpha_f4t4.png")

beta_1 = band_power(eeg1, f=band_frq(eeg1, band=:beta))
beta_2 = band_power(eeg2, f=band_frq(eeg2, band=:beta))
beta1_f4 = beta_1[f4, :]
beta2_f4 = beta_2[f4, :]
Plots.plot(beta1_f4, seriestype=:boxplot, legend=nothing, title="band: beta, location: F4", ylabel="power [dB]")
p1 = plot!(beta2_f4, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15))
beta1_t4 = beta_1[t4, :]
beta2_t4 = beta_2[t4, :]
Plots.plot(beta1_t4, seriestype=:boxplot, legend=nothing, title="band: beta, location: T4", ylabel="power [dB]")
p2 = plot!(beta2_t4, seriestype=:boxplot, xticks=(1:2, ["V1", "V2"]), ylims=(0, 15), ylabel="")
beta_plot = Plots.plot(p1, p2, fillcolor=:white, markercolor=:white, markersize=2, guidefontsize=8, tickfontsize=6, titlefontsize=10, dpi=300)
plot_save(beta_plot, file_name="beta_f4t4.png")

# plot results F4 / T4
p_f4t4 = Plots.plot(delta_plot, theta_plot, alpha_plot, beta_plot, layout=(2, 2), titlefontsize=5, tickfontsize=4, guidefontsize=4, dpi=300)
plot_save(p_f4t4, file_name="f4t4.png")

b1 = ["delta1_f3", "delta1_t3", "delta1_f4", "delta1_t4", "theta1_f3", "theta1_t3", "theta1_f4", "theta1_t4", "alpha1_f3", "alpha1_t3", "alpha1_f4", "alpha1_t4", "beta1_f3", "beta1_t3", "beta1_f4", "beta1_t4"]
b2 = ["delta2_f3", "delta2_t3", "delta2_f4", "delta2_t4", "theta2_f3", "theta2_t3", "theta2_f4", "theta2_t4", "alpha2_f3", "alpha2_t3", "alpha2_f4", "alpha2_t4", "beta2_f3", "beta2_t3", "beta2_f4", "beta2_t4"]
for idx in eachindex(b1)
    NeuroAnalyzer.summary(eval(Meta.parse(b1[idx])), eval(Meta.parse(b2[idx])), g1=b1[idx], g2=b2[idx]);
    _, t, _, df, p = cmp_test(eval(Meta.parse(b1[idx])), eval(Meta.parse(b2[idx])), paired=false);
    make_table(header=["$(b1[idx]) vs $(b2[idx])" ""], data=["test statistic" t; "df" df; "p" p])
    _, r, _, _, df, p = cor_test(eval(Meta.parse(b1[idx])), eval(Meta.parse(b2[idx])));
    make_table(header=["cor $(b1[idx]), $(b2[idx])" ""], data=["df" df; "r" r; "p" p])
    println("")
    println("────────────────────────────────────────────────────────────────────────────")
    println("")
end
println("alpha = 0.05 / $(length(b1)) = $(0.05 / length(b1))")

delta_f3_change = delta2_f3 - delta1_f3
delta_f4_change = delta2_f4 - delta1_f4
delta_t3_change = delta2_t3 - delta1_t3
delta_t4_change = delta2_t4 - delta1_t4
theta_f3_change = theta2_f3 - theta1_f3
theta_f4_change = theta2_f4 - theta1_f4
theta_t3_change = theta2_t3 - theta1_t3
theta_t4_change = theta2_t4 - theta1_t4
alpha_f3_change = alpha2_f3 - alpha1_f3
alpha_f4_change = alpha2_f4 - alpha1_f4
alpha_t3_change = alpha2_t3 - alpha1_t3
alpha_t4_change = alpha2_t4 - alpha1_t4
beta_f3_change = beta2_f3 - beta1_f3
beta_f4_change = beta2_f4 - beta1_f4
beta_t3_change = beta2_t3 - beta1_t3
beta_t4_change = beta2_t4 - beta1_t4

b1 = ["delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change"]
b2 = ["delta_f4_change", "delta_t3_change", "delta_t4_change", "theta_f3_change", "theta_f4_change", "theta_t3_change", "theta_t4_change", "alpha_f3_change", "alpha_f4_change", "alpha_t3_change", "alpha_t4_change", "beta_f3_change", "beta_f4_change", "beta_t3_change", "beta_t4_change"]
for idx in eachindex(b1)
    NeuroAnalyzer.summary(eval(Meta.parse(b1[idx])), eval(Meta.parse(b2[idx])), g1=b1[idx], g2=b2[idx]);
    _, r, _, _, df, p = cor_test(eval(Meta.parse(b1[idx])), eval(Meta.parse(b2[idx])));
    make_table(header=["cor $(b1[idx]), $(b2[idx])" ""], data=["df" df; "r" r; "p" p])
    println("")
    println("────────────────────────────────────────────────────────────────────────────")
    println("")
end
println("alpha = 0.05 / $(length(b1)) = $(0.05 / length(b1))")

delta_f3_change = delta2_f3 - delta1_f3
delta_t3_change = delta2_t3 - delta1_t3
theta_t3_change = theta2_t3 - theta1_t3
alpha_t3_change = theta2_t3 - theta1_t3
beta_t3_change = beta2_t3 - beta1_t3
b1 = ["delta_f3_change", "delta_f3_change", "delta_f3_change", "delta_f3_change"]
b2 = ["delta_t3_change", "theta_t3_change", "alpha_t3_change", "beta_t3_change"]
for idx in eachindex(b1)
    NeuroAnalyzer.summary(eval(Meta.parse(b1[idx])), eval(Meta.parse(b2[idx])), g1=b1[idx], g2=b2[idx]);
    _, r, _, _, df, p = cor_test(eval(Meta.parse(b1[idx])), eval(Meta.parse(b2[idx])));
    make_table(header=["cor $(b1[idx]), $(b2[idx])" ""], data=["df" df; "r" r; "p" p])
    println("")
    println("────────────────────────────────────────────────────────────────────────────")
    println("")
end
println("alpha = 0.05 / $(length(b1)) = $(0.05 / length(b1))")

_, mfrq, _ = band_mpower(eeg1, ch=1:19, f=band_frq(eeg, band=:alpha))
p = plot_bar(mfrq[1:19, 1], xlabels=labels(eeg1)[1:19], ylabel="Frequency [Hz]", title="Maximum α band frequency\n[epoch: 1]")
_, mfrq, _ = band_mpower(eeg1, ch=1:19, f=band_frq(eeg, band=:alpha))
p = plot_bar(mfrq[1:19, 1], xlabels=labels(eeg2)[1:19], ylabel="Frequency [Hz]", title="Maximum α band frequency\n[epoch: 1]")